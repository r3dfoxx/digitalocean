<?php

namespace App\Listeners;

use App\Models\Server;
use App\Events\CreatingServer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreatingServer $event)
    {
        // $servers = Server::all();
        // foreach($servers as $server){
        //     Mail::to($server)->send();
        }
}
