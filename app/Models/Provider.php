<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Provider extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $table = 'providers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'remember_token',
        'user_id'
    ];

    // public $timestamps = true;

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    public function server()
    {
        return $this->hasMany(Server::class, 'provider_id', 'id');
    }
}
