<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Server extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_name',
        'datacenter',
        'status',
        'ip',
        'server_plan',

    ];
    public $timestamps = true;

    public function providers()
    {
        return $this->belongsTo(Provider::class);
    }
}
