<?php

namespace App\Http\Livewire;

use Livewire\Component;
use DigitalOceanV2\Client;

class Login extends Component
{
    public function render()
    {
        return view('livewire.login');
    }
}
