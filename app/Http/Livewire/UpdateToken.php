<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Provider;

class UpdateToken extends Component
{
    public $updatedToken;
    protected $listeners = ['getId'];

    public function getModelId($id)
    {
        $this->id = $id;
    }
        public function update()
        {
            $tokens = auth()->user()->provider()->update();
        }

    public function render()
    {

        return view('livewire.update-token');
    }
}
