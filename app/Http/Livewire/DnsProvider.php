<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use App\Models\Provider;
use App\Services\ProviderService;
use Illuminate\Support\Facades\Auth;

class DnsProvider extends Component
{
    public $token;
    public $updatedToken;
    public $selectItem;
    public $action;
    public $isOpen = 0;
    protected $provider;


    public function render()
    {
        $tokens = auth()->user()->provider()->get();
        return view('livewire.dns-provider', ['tokens' => $tokens]);
    }

    private function resetInputFields()
    {
        $this->token = '';
    }

    public function create()
    {
        $validatedData = $this->validate([
            'token' => 'required',
        ]);

        \App\Models\Provider::create([
            'user_id' => Auth::user()->id,
            'name' => 'Digital Ocean',
            'remember_token' => $this->token,
        ]);
        session()->flash('message', 'Key Created Successfully.');
        $this->resetInputFields();

    }

    public function delete($token)
    {
        \App\Models\Provider::destroy($token);
        session()->flash('message', 'Deleted');

    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    protected $listeners = ['edit'];

    public function selectItem($token, $action)
    {
        $this->tokens = $token;
        if($action == 'delete') {
            \App\Models\Provider::destroy($this->tokens);
            session()->flash('message', 'Deleted');
        } else {
            $this->emit('getId', $this->selectItem);
            $this->openModal();
        }
    }
}
