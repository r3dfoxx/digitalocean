<?php

namespace App\Http\Livewire;


use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SshKey extends Component
{

    public $name;
    public $key;
    public $updateMode = false;


    private function resetInputFields()
    {
        $this->name = '';
        $this->key = '';
    }

    public function store()
    {
        $validatedData = $this->validate([
            'name' => 'required',
            'key' => 'required',
        ]);

        \App\Models\SshKey::create([
            'user_id' => Auth::user()->id,
            'name' => $this->name,
            'access_key' => $this->key,
        ]);

        session()->flash('message', 'Key Created Successfully.');
        $this->resetInputFields();
    }

    public function edit($id)
    {
        $this->updateMode = true;
        $sshKey =  \App\Models\SshKey::where('id', $id)->first();
        $sshKey->id = $id;
        $sshKey->name = $this->name;
        $sshKey->access_key = $this->key;
    }

    public function update()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'key' => 'required',
        ]);

        if ($this->ssh_key_id) {
            $sshKey = \App\Models\SshKey::find($this->ssh_key_id);
            $sshKey->update([
                'name' => $this->name,
                'access_key' => $this->key,
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Key Updated Successfully.');
            $this->resetInputFields();
        }
    }

    public function delete($id)
    {
        \App\Models\SshKey::find($id)->delete();
        session()->flash('message', 'Deleted');
    }

    public function render()
    {
       $data = auth()->user()->ssh_key()->get();
       return view('livewire.ssh-key', ['data' => $data]);
    }
}
