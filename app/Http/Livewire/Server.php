<?php

namespace App\Http\Livewire;

use Livewire\Component;
use DigitalOceanV2\Client;
use App\Events\CreatingServer;



class Server extends Component
{
    public $name;
    public $serverPlan;
    public $avalableRegions;

    public function resetInputFields()
    {
        $this->name = '';
    }

    public function submit()
    {
        $sshKeys = auth()->user()->ssh_key()->get();
        foreach ($sshKeys as $key) {
            $userKey = $key->access_key;
        }

        $tokens = auth()->user()->provider()->get();

        foreach ($tokens as $token) {
            $client = new Client();
        }
        $client->authenticate($token->remember_token);

        $region = $client->region();
        $regions = $region->getAll();

        $droplet = $client->droplet();
        $droplets = $droplet->getAll();

        $name = $this->name;
        $serverPlan = $this->serverPlan;
        $avalableRegions = $this->avalableRegions;


        $serverOS = 'ubuntu-18-04-x64';
        $created = $droplet->create($name, $avalableRegions, $serverPlan, $serverOS, $userKey);

        $this->emit('serverAdded', [$name, $avalableRegions, $serverPlan]);
        $this->emitSelf('serverDeleted');

        foreach ($regions as $region) {
            foreach ($droplets as $droplet) {
                $serverStatus = $droplet->status;
                $serverUniqueId = $droplet->id;
            }
        }

        foreach ($droplet->networks as $serverInfo) {
            $serverAdress = $serverInfo->ipAddress;
        }

        $size = $client->size();
        $sizes = $size->getAll();
        foreach ($sizes as $plan) {
            $server =  new \App\Models\Server();
        }
        $server->provider_id = $serverUniqueId;
        $server->server_name = $name;
        $server->datacenter = $avalableRegions;
        $server->server_plan = $plan->memory . 'RAM' . $plan->disk . 'GB Disk' . $plan->vcpus . 'Cores' . $plan->priceMonthly . '$';
        $server->ip = $serverAdress;
        $server->status = $serverStatus;
        $server->save();
    }

    public function render()
    {
        $token = '';
        $client = new \DigitalOceanV2\Client();
        $tokens = auth()->user()->provider()->get();
        foreach ($tokens as $token)
            $client = new \DigitalOceanV2\Client();
            $client->authenticate($token->remember_token);

            $region = $client->region();
            $regions = $region->getAll();

            $size = $client->size();
            $sizes = $size->getAll();

            return view('livewire.server', [
                'sizes' => $sizes,
                'regions' => $regions,
                ]);

    }
}
