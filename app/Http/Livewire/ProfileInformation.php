<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ProfileInformation extends Component
{

    public $name;
    public $email;

    private function resetInputFields()
    {
        $this->name = null;
        $this->email = null;
    }

    public function update()
    {
        // $this->validate([
        //     'name' => 'required', 'string', 'max:255',
        //     'email' => 'required', 'email', 'max:255',
        //     ]);
        $user = Auth::user();

        if (Auth::user())
            $user = User::find(Auth::user()->id);
        $user->update([
            'name' => $this->name,
            'email' => $this->email,
        ]);
        $this->updateMode = false;
        $this->resetInputFields();
        session()->flash('message', 'User Information Updated Successfully.');
    }



    public function render()
    {
        return view('livewire.profile-information');
    }
}
