<?php

namespace App\Http\Livewire;


use App\Models\Server;
use Livewire\Component;
use DigitalOceanV2\Client;
use Livewire\WithPagination;
use DigitalOceanV2\ResultPager;
use Illuminate\Database\Eloquent\Builder;

class ServerTable extends Component
{

    use WithPagination;

    protected $listeners = ['serverAdded' => 'noop', 'serverDeleted' => '$refresh'];

    public function noop()
    {
        //
    }
    public function render()
    {
        $testing = '';
        $tokens = auth()->user()->provider()->get();

        foreach ($tokens as $token)

        $client = new Client();
        $client->authenticate($token->remember_token);
        $pager = new ResultPager($client);

        $droplet = $client->droplet();
        $droplets = $pager->fetchAll($client->droplet(), 'getAll');

        foreach ($droplets as $droplet){
            $droplet = $droplet->id;
        }

        $servers = \App\Models\Server::all();

        return view('livewire.server-table', [ 'droplet' => $droplet ,'servers' => \App\Models\Server::paginate(3) ]);
    }

    public function destroy($id)
    {

        if ($id) {
            $server = \App\Models\Server::where('id', $id);
            $server->delete();
        }
        $tokens = auth()->user()->provider()->get();

        foreach ($tokens as $token)
        $client = new Client();
        $client->authenticate($token->remember_token);

         $droplet = $client->droplet();
         $droplets = $droplet->getAll();

         foreach($droplets as $dropletIndex){
            $droplet->remove($dropletIndex->id);
         }
         return view('livewire.server-table',  ['serverDeleted' => '$refresh']);
        }

    }


