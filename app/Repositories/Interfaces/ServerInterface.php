<?php

namespace App\Repositories\Interfaces;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;


interface ServerInterface extends BaseInterface
{
    public function all();

    public function create();

    public function update(int $id, array $data);

    public function destroy(int $id);

    public function paginate();
}
