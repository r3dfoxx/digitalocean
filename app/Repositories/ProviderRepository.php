<?php

namespace App\Repositories;

use Throwable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\ProviderInterface;

class ProviderRepository extends BaseRepository implements ProviderInterface
{
    /**
     * @var Model
     */
    public $model;
    public $sortBy = 'id';
    public $sortOrder = 'desc';


    public function __construct(Provider $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * Create a new record in the database
     *
     * @param array $data
     * @return model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update record in the database and return status
     *
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function update(int $id, array $data): bool
    {
        $update = $this->model->where('id', $id)->update($data);

        return $update;
    }
    /**
     * Remove record from the database
     *
     * @param int $id
     * @return boolean
     */
    public function destroy(int $id): bool
    {
        $this->model->destroy($id);
        return true;
    }


    /**
     * Get the associated model
     *
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * Set the associated model
     *
     * @param $model
     * @return $this
     */

    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }
    // public function paginate()
    // {
    // return $this->model->paginate(10);
    // }
}
