@extends('layouts.app')


@if(count(auth()->user()->provider) == 0)
@section('content')
@livewire('header')
@livewire('dashboard')

@else

@section('content')
@livewire('header')

@livewire('server')
@livewire('server-table')
@endif

@endsection
