<div>
    <div class="md:grid md:grid-cols-3 md:gap-6"> {{--wire:id="QRmIZCXKNdt80MRK0EnY"--}}
        <div class="md:col-span-1">
            <div class="px-4 sm:px-0">
                <h3 class="text-lg font-medium text-gray-900">Dns Providers</h3>
                    <p class="mt-1 text-sm text-gray-600">Add Digital Ocean Account Token</p>
            </div>
        </div>
            <div class="mt-5 md:mt-0 md:col-span-2">
                @if(empty($userToken))
                    <form wire:submit.prevent="create">
                        <div class="shadow overflow-hidden sm:rounded-md">
                            <div class="px-4 py-5 bg-white sm:p-6">
                                @if(Session::has('message'))
                                    <div class="block text-sm text-left text-indigo-600 bg-indigo-200 border border-indigo-400 h-12 flex items-center p-4 rounded-sm"role="alert">
                                        <span class="glyphicon glyphicon-ok"></span>
                                            <em> {!! session('message') !!}</em>
                                    </div>
                                        @endif
                                            <div class="card">
                                                <div class="col-span-6 sm:col-span-4">
                                                    <label class="block font-medium text-sm text-gray-700" for="token">Token</label>
                                                        <input class="form-input rounded-md shadow-sm mt-1 block w-full" id="token" type="text"wire:model="token" placeholder="Add your application token">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                                            <button type="submit" class="border border-indigo-500 bg-indigo-500
                                            text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none
                                            hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
                                            wire: click=" create">Create</button>
                                        </div>
                                            <div class="card-body">
                                                <table class="table table-borderless m-b-none">
                                                    <thead>
                                                        <tr>
                                                            <th class="pd-1 bg-white-200">Token</th>
                                                            <th class="pd-1"></th>
                                                        </tr>
                                                    </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="d-flex justify-content-end pd-1">
                                                                    @foreach ($tokens as $userToken)
                                                                    <div class="btn-table-align">{{ $userToken->remember_token }}</div>

                                                                </td>
                                                                <td class="d-flex justify-content-end pd-1">
                                                                    <div class="btn-group-xs pull-right row">
                                                                        <button wire:click="selectItem({{ $userToken->id }} , 'update')" type="edit"
                                                                            class="bg-green-600 text-white rounded-md px-4 py-2 m-2 transition
                                                                            duration-500 ease select-none hover:bg-green-800 focus:outline-non
                                                                            ocus:shadow-outline">
                                                                                <span>Edit</span>
                                                                        </button>
                                                                        @if($isOpen)
                                                                        @include('livewire.update-token')
                                                                    @endif
                                                                        <button wire:click="selectItem({{ $userToken->id }} ,'delete')" type="delete"
                                                                            class="bg-red-600 text-white rounded-md px-4 py-2 m-2 transition
                                                                            duration-500 ease select-none hover:bg-red-800 focus:outline-non
                                                                            ocus:shadow-outline">
                                                                                <span>Delete</span>
                                                                        </button>
                                                                        @endforeach
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                         @else
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
