<div>
<div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8 bg-white shadow">
        <span class="inline w-32 font-bold">Provision A Digital Ocean Server</span>

        <form wire:submit.prevent="submit">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="form-group row">
                <label class="inline-block w-32 font-bold">Name:</label>
                <input type="text" class="p-2 px-4 py-2 pr-8 leading-tight bg-white border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline"
                    placeholder="Enter name" name="projectName" wire:model="name">
                </div>
                <br/>
                @if(count($sizes) > 0)
                <div class="form-group">
                    <div class="mb-8">
                        <label class="inline-block w-32 font-bold">Server Plan:</label>

                        <select name="city" wire:model="serverPlan"
                            class="p-2 px-4 py-2 pr-8 leading-tight bg-white border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline">
                            <option value=''>Choose a Server Plan</option>
                            @foreach($sizes as $plan)

                                <option value={{ $plan->slug}}>{{$plan->memory .'RAM' . $plan->disk .'GB Disk' . $plan->vcpus . 'Cores'. $plan->priceMonthly .'$'  }}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            @elseif($sizes == 'undefined')
            <div class="form-group">
                <div class="mb-8">
                    <label class="inline-block w-32 font-bold">Server Plan:</label>
                    <select name="city" wire:model="serverPlan"
                        class="p-2 px-4 py-2 pr-8 leading-tight bg-white border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline">
                        <option value=''>Choose a Server Plan</option>
                            <option value=></option>
                    </select>
                </div>
        </div>
        @endif
        @if(count($regions) > 0)
                <div class="form-group">
                    <div class="mb-8">
                        <label class="inline-block w-32 font-bold">Regions:</label>
                        <select name="avalableRegions" wire:model="avalableRegions"
                            class="p-2 px-4 py-2 pr-8 leading-tight bg-white border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline">
                            <option value=''>Choose a Regions</option>
                            @foreach($regions as $region)
                                <option value={{ $region->slug}}>{{$region->name}}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            @elseif($regions == 'undefined')
            <div class="form-group">
                <div class="mb-8">
                    <label class="inline-block w-32 font-bold">Regions:</label>
                    <select name="avalableRegions" wire:model="avalableRegions"
                        class="p-2 px-4 py-2 pr-8 leading-tight bg-white border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline">
                        <option value=''>Choose a Regions</option>
                        <option value=></option>
                    </select>
                </div>
            </div>
            @endif

                    <span class= "cinline-block w-32 font-bold">Default OS:</span>
                    <label class= "cinline-block w-32 font-bold">Ubuntu 18.04</label>
                    <hr />
                    <br />
                    <button class="block uppercase mx-2 shadow bg-green-500 hover:bg-green-700 focus:shadow-outline focus:outline-none text-white text-xs py-2 px-5 rounded submit">
                        <div wire:loading wire:target="submit">
                            <span class="">
                                <x-loading /></span>
                        </div>Create Server
                    </button>
            </form>

        <br />
    </div>
</div>

