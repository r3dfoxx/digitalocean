<div>
    <div class="md:grid md:grid-cols-3 md:gap-6" :key="time().QRmIZCXKNdt80MRK0EnY">
        <div class="md:col-span-1">
            <div class="px-4 sm:px-0">
                <h3 class="text-lg font-medium text-gray-900">Profile Information</h3>
                <p class="mt-1 text-sm text-gray-600">
                    Update your account's profile information and email address.
                </p>
            </div>
        </div>
        <div class="mt-5 md:mt-0 md:col-span-2">
            <form wire:submit.prevent="update">
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        @if(Session::has('message'))
                        <div class="block text-sm text-left text-indigo-600 bg-indigo-200 border border-indigo-400 h-12 flex items-center p-4 rounded-sm"
                            role="alert">
                            <span class="glyphicon glyphicon-ok"></span>
                            <em> {!! session('message') !!}</em>
                        </div>
                        @endif
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-4">
                                <label class="block font-medium text-sm text-gray-700" for="name">
                                    Name
                                </label>
                                <input class="form-input rounded-md shadow-sm mt-1 block w-full" id="name" type="text"
                            wire:model="name" autocomplete="name" placeholder="{{Auth::user()->name}}">
                            </div>
                            <div class="col-span-6 sm:col-span-4">
                                <label class="block font-medium text-sm text-gray-700" for="email">
                                    Email
                                </label>
                                <input class="form-input rounded-md shadow-sm mt-1 block w-full" id="email" type="email"
                                    wire:model="email" placeholder="{{Auth::user()->email}}">
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline
                            wire: click=" update"> {{--loading.attr="disabled"--}}
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
