@livewire('header')
<div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
    @livewire('profile-information')
    <br />
    @livewire('ssh-key')
    <br />
    @livewire('dns-provider')
