<div>
    <div class="md:grid md:grid-cols-3 md:gap-6">
        <div class="md:col-span-1">
            <div class="px-4 sm:px-0">
                <h3 class="text-lg font-medium text-gray-900">SSH key</h3>
                <p class="mt-1 text-sm text-gray-600">
                    Add public SSH keys
                </p>
            </div>
        </div>
        <div class="mt-5 md:mt-0 md:col-span-2">
            @if(Session::has('message'))
            <div class="block text-sm text-left text-indigo-600 bg-indigo-200 border border-indigo-400 h-12 flex items-center p-4 rounded-sm"
                role="alert">
                <span class="glyphicon glyphicon-ok"></span>
                <em> {!! session('message') !!}</em>
            </div>
            @endif
            <form wire:submit.prevent="store">
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">

                        <div class="grid grid-cols-6 gap-6">
                            <!-- Name -->
                            <div class="col-span-6 sm:col-span-4">
                                <label class="block font-medium text-sm text-gray-700" for="name">Name</label>
                                    <input class="form-input rounded-md shadow-sm mt-1 block w-full" id="name" type="text"wire:model="name" autocomplete="name" placeholder="Add  SSH key name" >
                            </div>
                            <div class="col-span-6 sm:col-span-4">
                                <label class="block font-medium text-sm text-gray-700" for="key">Public Key</label>
                                <textarea class="form-input rounded-md shadow-sm mt-1 block w-full" id="key" type="key"
                                    wire:model.="key" rows="7" placeholder="Add your public SSH key here"></textarea>
                            </div>
                        </div>

                    <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit"
                            class="border border-green-500 bg-green-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-600 focus:outline-none focus:shadow-outline">
                            <span>
                                Add Key
                            </span>
                        </button>
                    </div>
            </form>
            <body class="antialiased font-sans bg-gray-200">
                <div class="container mx-auto px-4 sm:px-8">
                    <div class="py-8">
                        <div>
                            <h2 class="text-2xl font-semibold leading-tight">SSH Keys</h2>
                        </div>
                        <div class="table-responsive">
                            <table class="min-w-full leading-normal table">
                                @if(empty( $data ))
                                <thead>
                                    <tr>
                                        <th
                                            class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider col-3">
                                            Name</th>
                                        <th
                                            class="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Action</th>
                                    </tr>
                                </thead>
                                @else
                                <tbody>
                                    <tr>
                                        @foreach( $data as $key )
                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p class="text-gray-900 whitespace-no-wrap">
                                                {{ $key->name }}
                                            </p>
                                        </td>
                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <button  wire:click="delete({{ $key->id }})" type="delete"
                                                    class="bg-red-600 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-red-800 focus:outline-none focus:shadow-outline">
                                                    <span>Delete</span>
                                            </button>
                                        </td>
                                        @endforeach
                                        @endif
                                    </tr>
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        </div>
    </div>
</div>
