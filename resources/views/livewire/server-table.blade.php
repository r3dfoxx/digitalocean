<div>
    <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8 bg-white shadow">
        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
        <div class="table-responsive">
            <table class="min-w-full leading-normal table">
                @if(empty( $servers || $droplet ))
                <x-table--header />
                @else
                <x-table--header />
                <tbody>
                    <tr>
                        @foreach( $servers as $server )
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 whitespace-no-wrap"> {{ $server->server_name }}</p>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 whitespace-no-wrap"> {{ $server->datacenter }}</p>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <p class="text-gray-900 whitespace-no-wrap"></p> {{ $server->ip }}
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                                <span class="relative"></span> {{ $server->status }}
                            </span>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <button wire:click="destroy({{ $server->id }})" wire:click="$refresh"
                                class="block uppercase mx-2 shadow bg-red-500 hover:bg-red-700 focus:shadow-outline focus:outline-none text-white text-xs py-2 px-5 rounded">
                                <div wire:loading wire:target="delete">
                                    <span class="">
                                        <x-loading /></span>
                                </div>Delete Server
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                @endif
            </table>
            {{ $servers->links() }}
        </div>
    </div>
</div>
</div>
    <script>
        Pusher.logToConsole = true;

        var pusher = new Pusher('b5c1c869e5b9fb161150', {
            cluster: 'eu'
        });
        var channel = pusher.subscribe('test');
        channel.bind(['CreatingServer'], (data) => {
        alert(JSON.stringify(data));
        })
    </script>
