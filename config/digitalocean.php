<?php

declare(strict_types=1);

/*
 * This file is part of Laravel DigitalOcean.
 *
 * (c) Graham Campbell <graham@alt-three.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | DigitalOcean Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. An example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'token'   => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOGI5MjEyYTE4M2NmMTVlYTEzYmM4ZjU2ZjNlYTAyNTEwZmUxYTRiN2Q4YzQ0MjZhOWViNDA1NzFmMDE5YWY1OTQwZmYwMDYwNmNkMDNkODMiLCJpYXQiOjE2MDQ5NTE2MjAsIm5iZiI6MTYwNDk1MTYyMCwiZXhwIjoxNjM2NDg3NjIwLCJzdWIiOiIyMyIsInNjb3BlcyI6W119.SjaHNCGGKwNaneJLWD3yR-two15Yjfpog-rXztuq4b9WmLY-1iHJLOdpPxHW8Mz4-vRalVbpTM98jNGVcET7JFclCe5YjLE2uCCpPzKMuAp799VGWPHXw3TB-bf4lA6EEZ2BzWULD9dF4cZ3ADW8miMaqKbs61bMGA74PHEF9AeKav7JlI8050VrEDx2GlenqzEWJatuM6iyKBSDlXOZ3_60V7-BQydvRxDlM7uIioNsG5AAokPuE65zNrFmS_Kj760J1gCc8NmZeZJ3XFDafxHQn51TzLVQARELlp7ztGePgA53xm0ze1z1ROvKezwaM_W5cLjpJU83KLaEJBaz84r1Y5lLVZtUdiYWXPSK9fbeWe8-Uc-LqZF-szszyDdfiBBDfbG8y6ZoGQqoEtZWXV2f7K5w-hqlCHKGZ7Czyzu69tEWygAh7JAJaPv8jfVoiUfFEXes3CPxREEhQsqY2f0kWWnNjUsHyldNT27_Iy_S4Cxpjap5yjLEhjBh6RCLinu5oBh5xCONNkENUPAq2f_HJ8iFAOadMSpEbChgATyQ3qdjx1sCoJoPpBfM8vGR3Uyi5uzIH9QYfOhFChpjFs9fCksiwc9Enp79adbB9TZiL2TE5zwrWp7Zn2g3HiA60eOdIdLIYYSfYE5_ovov0k93oG9lQCI8HpVR2v8GP7g',
            'method'  => 'token',
        ],

    ],

];
