<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SshKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\SshKey::factory(40)->create();
    }
}
