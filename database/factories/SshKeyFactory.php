<?php

namespace Database\Factories;

use App\Models\SshKey;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class SshKeyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SshKey::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigit(),
            'name' => $this->faker->name(),
            'access_key' => Str::random(255),
            'secret_key' => Str::random(255),
        ];
    }
}
