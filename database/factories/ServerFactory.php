<?php

namespace Database\Factories;

use App\Models\Server;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Server::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'provider_id' => '1',
            'server_name' => $this->faker->company(),
            'ip' => $this->faker->ipv4(),
            'datacenter' => $this->faker->country(),
            'server_plan'=> $this->faker->randomElement($array = array ('Simply','Premium','Business')),
            'status' => 'active',
        ];
    }
}
